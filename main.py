from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore
from PyQt5 import QtGui
# Definition of the main window class
from mainWindow import Ui_MainWindow
# Definition of the other windows
from simulationWindow_process import Ui_GraphicsWindow_process
from simulationWindow_1mode import Ui_GraphicsWindow_1mode
from simulationWindow_both import Ui_GraphicsWindow_both
from infoWindow import Ui_InfoWindow
# Simulator core functions
from core_functions import conversion, \
                response, indexes, graph, \
                fig1, fig2, fig3, fig4
# Numpy
import numpy as np
# Control
import control
# To show math expressions
from sympy import denom, numer, preview, sec
# Palette for light and dark mode
import paletteModes


class SimulatorWindow(qtw.QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)        
        self.setupUi(self)     
        self.menubar.setStyleSheet(" QToolBar {background: rgb(0, 0, 0) }")
        self.resetValues("all", True)  
        self.resize(1000, 0)
        self.current_simulationType = 0
        self.graphicsWindow = qtw.QMainWindow()
        self.infoWindow = qtw.QMainWindow() 
        self.rtSimulationActive = False
        self.results = ''
        self.currentColorMode = 'dark'
        self.toggleColors()        
        self.setUpInputs()    
        self.detectChanges()
        self.aboutText = open("text/about.html", "r").read()
        self.helpText = open("text/help.html", "r").read()

        self.generateDiscreteMathExpression('p')
        self.generateDiscreteMathExpression('c')


    def openInfoWindow(self, type):
        ''' Opens the 'about' or 'help' window. '''

        self.info_ui = Ui_InfoWindow()            
        self.info_ui.setupUi(self.infoWindow)
        if type == 'help':
            _translate = QtCore.QCoreApplication.translate
            self.info_ui.textBrowser.setHtml(_translate("InfoWindow", self.helpText))
        elif type == 'about':
            _translate = QtCore.QCoreApplication.translate
            self.info_ui.textBrowser.setHtml(_translate("InfoWindow", self.aboutText))

        if self.currentColorMode == "dark":
            palette = paletteModes.dark()     
            brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
            self.info_ui.textBrowser.setStyleSheet(
                "color: rgb(255, 255, 255);" \
                "background-color: rgb(0, 0, 0);")
               
        elif self.currentColorMode == "light":
            palette = paletteModes.light()  
            brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))  
            self.info_ui.textBrowser.setStyleSheet(
                "background-color: rgb(255, 255, 255);")               
            
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)   
        self.infoWindow.setPalette(palette)

        self.infoWindow.resize(700,700)
        self.infoWindow.show()


    def setTestValues(self):
        ''' Sets input values for 
        testing purposes. '''

        self.simulationTimeEdit.setText("30")
        self.inputTimeEdit.setText("10")
        self.stepMag_edit.setText("1")
        self.rampSlope_edit.setText("1")
        self.process_num_edit.setText("1,2")
        self.process_den_edit.setText("1,2,3")
        self.process_deadTime_edit.setText("2")
        self.contr_num_edit.setText("1,2")
        self.contr_den_edit.setText("1,2,3")
        self.processP1Edit.setText("18")
        self.processP2Edit.setText("34")
        self.processP3Edit.setText("2")
        self.processP4Edit.setText("35")
        self.contrP1Edit.setText("33")
        self.contrP2Edit.setText("18")
        self.contrP3Edit.setText("26")
        self.contrP4Edit.setText("38")

        if self.current_simulationType == "Discrete" or \
        self.current_simulationType == 0:
            self.generateDiscreteMathExpression('p')
            self.generateDiscreteMathExpression('c')
        else:
            self.generateRealTimeMathExpression('p')
            self.generateRealTimeMathExpression('c')        


    def toggleColors(self):
        ''' Toggles between light
        and dark mode. '''

        if self.currentColorMode == "light":
            palette = paletteModes.dark()
            self.currentColorMode = "dark"
            self.actionToggleColors.setText("Light Mode")            
            self.LaTeXcolor = '1.0 1.0 1.0'            

        elif self.currentColorMode == "dark":
            palette = paletteModes.light()            
            self.currentColorMode = "light"
            self.actionToggleColors.setText("Dark Mode")
            self.LaTeXcolor = '0.0 0.0 0.0'

        self.setPalette(palette)
        self.graphicsWindow.setPalette(palette)
        if self.current_simulationType == "Discrete" or \
        self.current_simulationType == 0:
            self.generateDiscreteMathExpression('p')
            self.generateDiscreteMathExpression('c')
        else:
            self.generateRealTimeMathExpression('p')
            self.generateRealTimeMathExpression('c')


    def updateParameterNames(self, type):
        ''' Sets the name of each parameter, 
        depending on the process and controller 
        types. '''

        if type == 'p':
            processType = self.procTypeCombo.currentText()
            if processType == 'Standard':
                self.label_8.setText('DC Gain')
                self.label_9.setText('Natural Frequency')
                self.label_13.setText('Damping Factor')
                self.label_14.setText('Dead Time')
                self.processP3RangeButton.show()
                self.processP3RangeEdit.show()
                self.setSliderRange(self.processP3Slider, 
                    self.getRangeValues(self.processP3RangeEdit, 'p3'))
            elif processType == 'Alternative':
                self.label_8.setText('DC Gain')
                self.label_9.setText('Time Constant')
                self.label_13.setText('a')
                self.label_14.setText('Dead Time')
                self.processP3RangeButton.hide()
                self.processP3RangeEdit.hide()
                self.setSliderRange(self.processP3Slider, [0,1/0.1,1])
                self.processP3SliderValues = [0, 1, 0.1]
                

        elif type == 'c':
            pidType = self.pidCombo.currentText()
            if pidType == 'Standard':
                self.label_18.setText('Kp')
                self.label_19.setText('Ti')
                self.label_20.setText('Td')
                self.label_21.setText('α')
            if pidType == 'Series':
                self.label_18.setText('Kp\'')
                self.label_19.setText('Ti\'')
                self.label_20.setText('Td\'')
                self.label_21.setText('α\'')
            if pidType == 'Parallel':
                self.label_18.setText('Kp')
                self.label_19.setText('Ki')
                self.label_20.setText('Kd')
                self.label_21.setText('α')
            

    def generateRealTimeMathExpression(self, type):
        ''' Genererates LaTeX expressions for 
        the process and controller functions for
        real time simulations. '''

        file_name = 'img/function.png'

        if type[0] == 'p':
            p1 = self.processP1Edit.text()
            p2 = self.processP2Edit.text()
            p3 = self.processP3Edit.text()
            p4 = self.processP4Edit.text()
        elif type[0] == 'c':
            p1 = self.contrP1Edit.text()
            p2 = self.contrP2Edit.text()
            p3 = self.contrP3Edit.text()
            p4 = self.contrP4Edit.text()

        processType = self.procTypeCombo.currentText()
        if type == 'p' and processType != "Custom":
            try:                
                if processType == 'Standard': # process - standard
                    expression = 'P(s) = $\\frac{ ' + str(float(p1)*float(p2)**2) + ' } \
                                { s^2+' + str(2*float(p3)*float(p2)) + 's+' + str(float(p2)**2) + '}' + \
                                    'e^{-' + p4 + 's}$'
                    normalExpression = f"P(s) = {float(p1)*float(p2)**2} / (s^2 + {2*float(p3)*float(p2)}s + {float(p2)**2}) * e^(-{p4}s)"
                elif processType == 'Alternative': # process - alternative
                    expression = f'P(s) = $\\frac{{{p1}}} \
                                {{({float(p3)*float(p2)}s+1)({p2}s+1)}} \
                                    e^{{-{p4}s}}$'
                    normalExpression = f"P(s) = {p1} / (({float(p3)*float(p2)}s+1)({p2}s+1)) e^(-{p4}s)"
            except:
                expression = 'P(s) = '
                normalExpression = "P(s) = "

        elif type == 'p' and processType == "Custom":
            self.generateDiscreteMathExpression('p')
            return

        elif type == 'c':
            try:
                pidType = self.pidCombo.currentText()

                if pidType == 'Standard': #controller - standard
                    product = "{:.3f}".format(float(p4)*float(p3))
                    expression = f'C(s) = $ {p1} [ 1 + \\frac{{1}}{{{p2}s}} + \\frac{{{p3}s}}{{{product}s+1}}]$'
                    normalExpression = f"C(s) = {p1}[1 + 1/{p2}s + {p3}/({product}s+1)]"

                if pidType == 'Parallel':
                    product = "{:.3f}".format(float(p4)*float(p3))
                    expression = f'C(s) = $ {p1} + \\frac{{{p2}}}{{s}} + \\frac{{{p3}s}}{{{product}s+1}} $'
                    normalExpression = f"C(s) = {p1} + {p2}/s + {p3}s/({product}s+1)"

                if pidType == 'Series':
                    product = "{:.3f}".format(float(p4)*float(p3))
                    expression = f'C(s) = $ {p1} [ \\frac{{{p2}s+1}}{{{p2}s}}  \\frac{{{p3}s+1}}{{{product}s+1}} ]  $'
                    normalExpression = f"C(s) = {p1}[ ({p2}s+1)/({p2}s) * ({p3}s+1)/({product}s+1) ]"
            except:
                expression = "C(s) = "
                normalExpression = "C(s) = "

        try:
            preview(expression, viewer='file', filename=file_name, euler=False, 
                    dvioptions=["-z", "0", "-D 250", "-bg", "Transparent", 
                                f"-fg rgb {self.LaTeXcolor}'"])
            if type[0] == 'p':
                self.processFunction_label.setPixmap(QtGui.QPixmap('img/function.png'))
            else:
                self.controllerFunction_label.setPixmap(QtGui.QPixmap('img/function.png'))
        except: 
            if type[0] == 'p':
                self.processFunction_label.setText(normalExpression)
            else:
                self.controllerFunction_label.setText(normalExpression)

        


    def generateDiscreteMathExpression(self, type):
        ''' Genererates LaTeX expressions for the process 
        and controller functions for discrete simulations. '''

        file_name = 'img/function.png'

        if type == 'p':
            numerator = self.process_num_edit.text().split(",")
            denominator = self.process_den_edit.text().split(",")
        else:
            numerator = self.contr_num_edit.text().split(",")
            denominator = self.contr_den_edit.text().split(",")
        
        if numerator[0] != '':
            try:
                i = len(numerator)-1
                numString = ''
                for num in numerator:
                    if float(num) != 0 and float(num) != 1 and i != 0 and i != 1:
                        numString += (num + "s^" + str(i) + "+")
                    elif float(num) == 1 and i != 0 and i != 1:
                        numString += ("s^" + str(i) + "+")
                    elif float(num) != 0 and i == 0:
                        numString += num
                    elif float(num) != 0 and float(num) != 1 and i == 1:
                        numString += (num + "s+")
                    elif float(num) == 1 and i == 1:
                        numString += "s+" 
                    elif float(num) == 0:
                        pass
                    i -= 1
                if numString[-1] == "+":
                    numString = numString[:-1] # Removes extra "+"
            except:
                numString = ''
                qtw.QMessageBox.information(self, "Invalid numerator", 
                            "Correct format: 1,2,3,4...")

        else:
            numString = ''
        
        if denominator[0] != '':
            try:
                i = len(denominator)-1
                denString = ''
                for num in denominator:
                    if float(num) != 0 and float(num) != 1 and i != 0 and i != 1:
                        denString += (num + "s^" + str(i) + "+")
                    elif float(num) == 1 and i != 0 and i != 1:
                        denString += ("s^" + str(i) + "+")
                    elif float(num) != 0 and i == 0:
                        denString += num
                    elif float(num) != 0 and float(num) != 1 and i == 1:
                        denString += (num + "s+")
                    elif float(num) == 1 and i == 1:
                        denString += "s+" 
                    elif float(num) == 0:
                        pass
                    i -= 1
                if denString[-1] == "+":
                    denString = denString[:-1] # Removes extra "+"
            except:
                denString = ''
                qtw.QMessageBox.information(self, "Invalid denominator", 
                            "Correct format: 1,2,3,4...")
        else:
            denString = ''

        procDT = self.process_deadTime_edit.text()

        try:
            float(procDT)
            if type == 'p':
                expression = 'P(s) = $\\frac{ ' + numString + ' }{ ' + denString + '}e^{-' + procDT + 's}$'   
                normalExpression = f"P(s) = ({numString})/({denString})*e^(-{procDT}s)"         
            else:
                expression = 'C(s) = $\\frac{ ' + numString + ' }{ ' + denString + '}$'
                normalExpression = f"P(s) = ({numString})/({denString})"

            try:
                preview(expression, viewer='file', filename=file_name, euler=False, 
                        dvioptions=["-z", "0", "-D 250", "-bg", "Transparent", 
                                    f"-fg rgb {self.LaTeXcolor}'"])
                if type == 'p':
                    self.processFunction_label.setPixmap(QtGui.QPixmap('img/function.png'))
                else:
                    self.controllerFunction_label.setPixmap(QtGui.QPixmap('img/function.png'))
            except:
                if type == 'p':
                    self.processFunction_label.setText(normalExpression)
                else:
                    self.controllerFunction_label.setText(normalExpression)
        except:
            qtw.QMessageBox.information(self, "Invalid dead time", 
                            "Dead time must be a number")


    def openGraphsWindow(self, img1='img/transparent.png', 
                img2='img/transparent.png', img3='img/transparent.png', 
                img4='img/transparent.png', realTime=False):
        
        ''' Opens the simulation Window, containing the graphs
        and performance parameter values.  '''

        if not self.rtSimulationActive:    
            if self.current_mode == "Regulatory control" \
            or self.current_mode == "Servo control":
                self.ui = Ui_GraphicsWindow_1mode()
            elif self.current_mode == "Servo and regulatory":
                self.ui = Ui_GraphicsWindow_both()
            else:
                self.ui = Ui_GraphicsWindow_process()

            self.ui.setupUi(self.graphicsWindow)            
        
        try:
            self.ui.label.setPixmap(QtGui.QPixmap(img1))
            self.ui.label_2.setPixmap(QtGui.QPixmap(img2))
            self.ui.label_3.setPixmap(QtGui.QPixmap(img3))
            self.ui.label_4.setPixmap(QtGui.QPixmap(img4))
        except:
            pass

        try:
            self.ui.openFig1.clicked.connect(lambda: self.openFigure(fig1))
            self.ui.openFig2.clicked.connect(lambda: self.openFigure(fig2))
            self.ui.openFig3.clicked.connect(lambda: self.openFigure(fig3))
            self.ui.openFig4.clicked.connect(lambda: self.openFigure(fig4))
        except:
            pass

        self.ui.textEdit.setText(self.results)

        if self.currentColorMode == "dark":
            palette = paletteModes.dark()           
        elif self.currentColorMode == "light":
            palette = paletteModes.light()  
        self.graphicsWindow.setPalette(palette)
        
        self.graphicsWindow.resize(0,0)

        if not self.rtSimulationActive:  
            if realTime: 
                self.rtSimulationActive = True 
            self.graphicsWindow.show()


    def openFigure(self, figure):
        ''' Shows a figure with Matplotlib. '''

        figure.show()


    def resetValues(self, section, first=False):
        ''' Sets the default values for the inputs. '''

        # self.LaTeXcolor = ''

        if section == "sim_data" or section == "all":        
            self.simulationTimeEdit.setText(str(0))
            self.inputTimeEdit.setText(str(0))
            self.stepMag_edit.setText(str(0))
            self.rampSlope_edit.setText(str(0))
            self.simulationTimeEdit.setText(str(0))
            self.inputTimeEdit.setText(str(0))
            self.stepMag_edit.setText(str(0))
            self.rampSlope_edit.setText(str(0))

        if section == "all":
            self.padeDegree_edit.setText(str(10))
        
        if section == "discProcess" or section == "all":
            self.process_num_edit.setText("")
            self.process_den_edit.setText("")
            self.process_deadTime_edit.setText(str(0))
        
        if section == "discController" or section == "all":
            self.contr_num_edit.setText("")
            self.contr_den_edit.setText("")

        if section == "RTProcess" or section == "all":
            self.processP1Edit.setText("0")
            self.processP2Edit.setText("0")
            self.processP3Edit.setText("0")
            self.processP4Edit.setText("0")        
            self.processP1RangeEdit.setText("0,100,1")
            self.processP2RangeEdit.setText("0,100,1")
            self.processP3RangeEdit.setText("0,10,1")
            self.processP4RangeEdit.setText("0,100,1")

            self.processP1SliderValues = [0, 100, 1]
            self.processP2SliderValues = [0, 100, 1]
            self.processP3SliderValues = [0, 10, 1]
            self.processP4SliderValues = [0, 100, 1]

        if section == "RTController" or section == "all":
            self.contrP1Edit.setText("0")
            self.contrP2Edit.setText("0")
            self.contrP3Edit.setText("0")
            self.contrP4Edit.setText("0")
            self.contrP1RangeEdit.setText("0,100,1")
            self.contrP2RangeEdit.setText("0,100,1")
            self.contrP3RangeEdit.setText("0,100,1")
            self.contrP4RangeEdit.setText("0,100,1")
            
            self.contrP1SliderValues = [0, 100, 1]
            self.contrP2SliderValues = [0, 100, 1]
            self.contrP3SliderValues = [0, 100, 1]
            self.contrP4SliderValues = [0, 100, 1]

        if not first:
            if self.current_simulationType == "Discrete":
                self.generateDiscreteMathExpression('p')
                self.generateDiscreteMathExpression('c')
            else:
                self.generateRealTimeMathExpression('p')
                self.generateRealTimeMathExpression('c')


    def detectChanges(self):
        ''' Detects value changes on the sliders. '''

        self.processP1Slider.valueChanged.connect(lambda: 
            self.runRealTimeSimulation())
        self.processP1Slider.valueChanged.connect(lambda: 
            self.generateRealTimeMathExpression('p'))
        self.processP2Slider.valueChanged.connect(lambda: 
            self.runRealTimeSimulation())
        self.processP2Slider.valueChanged.connect(lambda: 
            self.generateRealTimeMathExpression('p'))
        self.processP3Slider.valueChanged.connect(lambda: 
            self.runRealTimeSimulation())
        self.processP3Slider.valueChanged.connect(lambda: 
            self.generateRealTimeMathExpression('p'))
        self.processP4Slider.valueChanged.connect(lambda: 
            self.runRealTimeSimulation())
        self.processP4Slider.valueChanged.connect(lambda: 
            self.generateRealTimeMathExpression('p'))

        self.contrP1Slider.valueChanged.connect(lambda: 
            self.runRealTimeSimulation())
        self.contrP1Slider.valueChanged.connect(lambda: 
            self.generateRealTimeMathExpression('c'))
        self.contrP2Slider.valueChanged.connect(lambda: 
            self.runRealTimeSimulation())
        self.contrP2Slider.valueChanged.connect(lambda: 
            self.generateRealTimeMathExpression('c'))
        self.contrP3Slider.valueChanged.connect(lambda: 
            self.runRealTimeSimulation())
        self.contrP3Slider.valueChanged.connect(lambda: 
            self.generateRealTimeMathExpression('c'))
        self.contrP4Slider.valueChanged.connect(lambda: 
            self.runRealTimeSimulation())
        self.contrP4Slider.valueChanged.connect(lambda: 
            self.generateRealTimeMathExpression('c'))


    def setUpInputs(self):
        ''' Sets up the functions that get executed
        when interacting with the interface. '''

        # Control Loop Operation
        self.controlLoop_group.buttonClicked.connect(self.setCurrentMode)
        self.current_mode = 0 
        
        # Tool bar buttons
        self.actionRun.triggered.connect(self.run_simulation)
        self.actionStop.triggered.connect(self.stopSimulation)
        self.actionReset_all_values.triggered.connect(lambda:
                                    self.resetValues('all'))
        self.actionTest.triggered.connect(self.setTestValues)
        self.actionToggleColors.triggered.connect(self.toggleColors)
        self.actionHelp.triggered.connect(lambda: self.openInfoWindow('help'))
        self.actionAbout.triggered.connect(lambda: self.openInfoWindow('about'))

        # Process and controller combo boxes        
        self.procTypeCombo.currentIndexChanged.connect(lambda: 
                    self.generateRealTimeMathExpression('p'))
        self.procTypeCombo.currentIndexChanged.connect(lambda: 
                    self.hideGeneralDataSection())
        self.procTypeCombo.currentIndexChanged.connect(lambda: 
                    self.runRealTimeSimulation()) 
        self.procTypeCombo.currentIndexChanged.connect(lambda: 
                    self.updateParameterNames('p')) 
        
        self.pidCombo.currentIndexChanged.connect(lambda:
                    self.generateRealTimeMathExpression('c'))
        self.pidCombo.currentIndexChanged.connect(lambda:
                    self.runRealTimeSimulation()) 
        self.pidCombo.currentIndexChanged.connect(lambda:
                    self.updateParameterNames('c')) 

        # Simulation data
        self.current_inputType = 0
        self.inputType_group.buttonClicked.connect(self.hideStepOrRamp)
        self.inputType_group.buttonClicked.connect(self.setCurrentInputType)
                            
        self.simulationType_group.buttonClicked.connect(self.setCurrentSimulationType)
        self.simulationType_group.buttonClicked.connect(self.hideGeneralDataSection)    
        self.simulationType_group.buttonClicked.connect(lambda: 
                self.generateDiscreteMathExpression('p') if self.current_simulationType == "Discrete"
                else self.generateRealTimeMathExpression('p'))
        self.simulationType_group.buttonClicked.connect(lambda: 
                self.generateDiscreteMathExpression('c') if self.current_simulationType == "Discrete"
                else self.generateRealTimeMathExpression('c'))
        self.resetSimDataValues_button.clicked.connect(
                    lambda: self.resetValues('sim_data')
        )


        # Bottom Buttons        
        self.run_button.clicked.connect(self.startSimulation)
        self.stop_button.clicked.connect(self.stopSimulation)    
        self.stop_button.setEnabled(self.rtSimulationActive)    
        self.resetAllValues_button.clicked.connect(lambda:
                            self.resetValues('all'))

        # Process Parameters
        self.process_num_edit.editingFinished.connect(lambda:
            self.generateDiscreteMathExpression('p'))
        self.process_den_edit.editingFinished.connect(lambda:
            self.generateDiscreteMathExpression('p'))
        self.process_deadTime_edit.editingFinished.connect(lambda:
            self.generateDiscreteMathExpression('p'))
        
        self.resetProcValues_button.clicked.connect(lambda:
                    self.resetValues('discProcess') if self.current_simulationType == "Discrete"
                    else self.resetValues('RTProcess'))

        self.processP1Edit.editingFinished.connect(lambda:
            self.generateRealTimeMathExpression('p'))
        self.processP2Edit.editingFinished.connect(lambda:
            self.generateRealTimeMathExpression('p'))
        self.processP3Edit.editingFinished.connect(lambda:
            self.generateRealTimeMathExpression('p'))
        self.processP4Edit.editingFinished.connect(lambda:
            self.generateRealTimeMathExpression('p'))


        # Sliders
        self.processP1Slider.valueChanged.connect(lambda: 
            self.showSliderValue(self.processP1Edit, 
                self.processP1Slider.value(), 
                self.processP1SliderValues))
        self.processP2Slider.valueChanged.connect(lambda: 
            self.showSliderValue(self.processP2Edit, 
                self.processP2Slider.value(), 
                self.processP2SliderValues))
        self.processP3Slider.valueChanged.connect(lambda: 
            self.showSliderValue(self.processP3Edit, 
                self.processP3Slider.value(), 
                self.processP3SliderValues))
        self.processP4Slider.valueChanged.connect(lambda: 
            self.showSliderValue(self.processP4Edit, 
                self.processP4Slider.value(), 
                self.processP4SliderValues))

        # Slider value line edits
        self.processP1Edit.textChanged.connect(lambda: 
            self.updateSliderValue(self.processP1Slider, 
                self.getLineEditValue(self.processP1Edit), 
                self.processP1SliderValues))
        self.processP2Edit.textChanged.connect(lambda: 
            self.updateSliderValue(self.processP2Slider, 
                self.getLineEditValue(self.processP2Edit), 
                self.processP2SliderValues))
        self.processP3Edit.textChanged.connect(lambda: 
            self.updateSliderValue(self.processP3Slider, 
                self.getLineEditValue(self.processP3Edit), 
                self.processP3SliderValues))
        self.processP4Edit.textChanged.connect(lambda: 
            self.updateSliderValue(self.processP4Slider, 
                self.getLineEditValue(self.processP4Edit), 
                self.processP4SliderValues))

        # Range buttons
        self.processP1RangeButton.clicked.connect(lambda: 
            self.setSliderRange(self.processP1Slider, 
                self.getRangeValues(self.processP1RangeEdit, 'p1')))
        self.processP2RangeButton.clicked.connect(lambda: 
            self.setSliderRange(self.processP2Slider, 
                self.getRangeValues(self.processP2RangeEdit, 'p2'))) 
        self.processP3RangeButton.clicked.connect(lambda: 
            self.setSliderRange(self.processP3Slider, 
                self.getRangeValues(self.processP3RangeEdit, 'p3'))) 
        self.processP4RangeButton.clicked.connect(lambda: 
            self.setSliderRange(self.processP4Slider, 
                self.getRangeValues(self.processP4RangeEdit, 'p4')))

        # Controller Parameters
        self.contr_num_edit.editingFinished.connect(lambda:
            self.generateDiscreteMathExpression('c'))        
        self.contr_den_edit.editingFinished.connect(lambda:
            self.generateDiscreteMathExpression('c'))
        
        self.resetContValues_button.clicked.connect(lambda:
                    self.resetValues('discController') if self.current_simulationType == "Discrete"
                    else self.resetValues('RTController'))

        self.contrP1Edit.editingFinished.connect(lambda:
            self.generateRealTimeMathExpression('c'))
        self.contrP2Edit.editingFinished.connect(lambda:
            self.generateRealTimeMathExpression('c'))
        self.contrP3Edit.editingFinished.connect(lambda:
            self.generateRealTimeMathExpression('c'))
        self.contrP4Edit.editingFinished.connect(lambda:
            self.generateRealTimeMathExpression('c'))


        # Sliders
        self.contrP1Slider.valueChanged.connect(lambda: 
            self.showSliderValue(self.contrP1Edit, 
                self.contrP1Slider.value(), 
                self.contrP1SliderValues))
        self.contrP2Slider.valueChanged.connect(lambda: 
            self.showSliderValue(self.contrP2Edit, 
                self.contrP2Slider.value(), 
                self.contrP2SliderValues))
        self.contrP3Slider.valueChanged.connect(lambda: 
            self.showSliderValue(self.contrP3Edit, 
                self.contrP3Slider.value(), 
                self.contrP3SliderValues))
        self.contrP4Slider.valueChanged.connect(lambda: 
            self.showSliderValue(self.contrP4Edit, 
                self.contrP4Slider.value(), 
                self.contrP4SliderValues))

        # Slider value line edits
        self.contrP1Edit.textChanged.connect(lambda: 
            self.updateSliderValue(self.contrP1Slider, 
                self.getLineEditValue(self.contrP1Edit), 
                self.contrP1SliderValues))
        self.contrP2Edit.textChanged.connect(lambda: 
            self.updateSliderValue(self.contrP2Slider, 
                self.getLineEditValue(self.contrP2Edit), 
                self.contrP2SliderValues))
        self.contrP3Edit.textChanged.connect(lambda: 
            self.updateSliderValue(self.contrP3Slider, 
                self.getLineEditValue(self.contrP3Edit), 
                self.contrP3SliderValues))
        self.contrP4Edit.textChanged.connect(lambda: 
            self.updateSliderValue(self.contrP4Slider, 
                self.getLineEditValue(self.contrP4Edit), 
                self.contrP4SliderValues))

        # Range buttons
        self.contrP1RangeButton.clicked.connect(lambda: 
            self.setSliderRange(self.contrP1Slider, 
                self.getRangeValues(self.contrP1RangeEdit, 'c1')))
        self.contrP2RangeButton.clicked.connect(lambda: 
            self.setSliderRange(self.contrP2Slider, 
                self.getRangeValues(self.contrP2RangeEdit, 'c2')))
        self.contrP3RangeButton.clicked.connect(lambda: 
            self.setSliderRange(self.contrP3Slider, 
                self.getRangeValues(self.contrP3RangeEdit, 'c3')))
        self.contrP4RangeButton.clicked.connect(lambda: 
            self.setSliderRange(self.contrP4Slider, 
                self.getRangeValues(self.contrP4RangeEdit, 'c4')))

        # Default values for the radio buttons
        self.radioButton_8.click()
        self.radioButton_17.click()
        self.radioButton_19.click()


    def startSimulation(self):
        ''' Calls the corresponding function to
        run a simulation, depending if it's real
        time of discrete. '''

        if (self.current_simulationType == "Discrete"):
            self.run_simulation()
        else:
            self.runRealTimeSimulation(first=True)
            self.stop_button.setEnabled(self.rtSimulationActive)


    def stopSimulation(self):
        ''' Sets the state of the real time 
        simulation to inactive. '''

        self.rtSimulationActive = False
        self.stop_button.setEnabled(self.rtSimulationActive)


    def setCurrentMode(self, object):
        ''' Sets the simulation mode, (servo control,
        regulatory control, both or process). '''

        self.current_mode = object.text()


    def setCurrentInputType(self, object):
        ''' Sets the input type (step or ramp). '''

        self.current_inputType = self.inputType_group.id(object)


    def setCurrentSimulationType(self, object):
        ''' Sets the simulation type 
        (real time or discrete). '''

        self.current_simulationType = object.text()


    def hideGeneralDataSection(self):
        ''' Shows/hides different inputs of the
        General Data Section according to the simulation
        type.
        '''

        #For discrete inputs
        discrete_simulation = self.current_simulationType == "Discrete"
        
        if discrete_simulation:
            # *** Discrete simulation inputs ***
            self.label_5.show()
            self.label_6.show()
            self.label_7.show()
            self.process_num_edit.show()
            self.process_den_edit.show()
            self.process_deadTime_edit.show()  
            self.contr_num_edit.show()
            self.contr_den_edit.show()
            self.label_16.show()
            self.label_17.show()

            # *** Real time simulation inputs ***
            self.label.hide()
            self.label_27.hide()
            self.procTypeCombo.hide()
            self.pidCombo.hide()
            # Sliders
            self.label_8.hide()
            self.label_9.hide()
            self.label_13.hide()
            self.label_14.hide()
            self.processP1Slider.hide()
            self.processP2Slider.hide()
            self.processP3Slider.hide()
            self.processP4Slider.hide()
            self.label_18.hide()
            self.label_19.hide()
            self.label_20.hide()
            self.label_21.hide()
            self.contrP1Slider.hide()
            self.contrP2Slider.hide()
            self.contrP3Slider.hide()
            self.contrP4Slider.hide()
            # Line edits
            self.processP1Edit.hide()
            self.processP2Edit.hide()
            self.processP3Edit.hide()
            self.processP4Edit.hide()
            self.contrP1Edit.hide()
            self.contrP2Edit.hide()
            self.contrP3Edit.hide()
            self.contrP4Edit.hide()
            # Buttons
            self.processP1RangeButton.hide()
            self.processP2RangeButton.hide()
            self.processP3RangeButton.hide()
            self.processP4RangeButton.hide()
            self.contrP1RangeButton.hide()
            self.contrP2RangeButton.hide()
            self.contrP3RangeButton.hide()
            self.contrP4RangeButton.hide()
            # Range line edits
            self.processP1RangeEdit.hide()
            self.processP2RangeEdit.hide()
            self.processP3RangeEdit.hide()
            self.processP4RangeEdit.hide()
            self.contrP1RangeEdit.hide()
            self.contrP2RangeEdit.hide()
            self.contrP3RangeEdit.hide()
            self.contrP4RangeEdit.hide()        
        else:
            processType = self.procTypeCombo.currentText()

            # Hide discrete controller parameters
            self.contr_num_edit.hide()
            self.contr_den_edit.hide()
            self.label_16.hide()
            self.label_17.hide()

            # Show/hide discrete/RT process parameters
            if (processType == "Custom"):
                # Discrete
                self.label_5.show()
                self.label_6.show()
                self.label_7.show()
                self.process_num_edit.show()
                self.process_den_edit.show()
                self.process_deadTime_edit.show()  

                # Real time
                self.label_8.hide()
                self.label_9.hide()
                self.label_13.hide()
                self.label_14.hide()
                self.processP1Slider.hide()
                self.processP2Slider.hide()
                self.processP3Slider.hide()
                self.processP4Slider.hide()

                self.processP1Edit.hide()
                self.processP2Edit.hide()
                self.processP3Edit.hide()
                self.processP4Edit.hide()

                self.processP1RangeButton.hide()
                self.processP2RangeButton.hide()
                self.processP3RangeButton.hide()
                self.processP4RangeButton.hide()

                self.processP1RangeEdit.hide()
                self.processP2RangeEdit.hide()
                self.processP3RangeEdit.hide()
                self.processP4RangeEdit.hide()
            else:
                self.label_5.hide()
                self.label_6.hide()
                self.label_7.hide()
                self.process_num_edit.hide()
                self.process_den_edit.hide()
                self.process_deadTime_edit.hide()  

                # Real time
                self.label_8.show()
                self.label_9.show()
                self.label_13.show()
                self.label_14.show()
                self.processP1Slider.show()
                self.processP2Slider.show()
                self.processP3Slider.show()
                self.processP4Slider.show()

                self.processP1Edit.show()
                self.processP2Edit.show()
                self.processP3Edit.show()
                self.processP4Edit.show()

                self.processP1RangeButton.show()
                self.processP2RangeButton.show()
                self.processP3RangeButton.show()
                self.processP4RangeButton.show()

                self.processP1RangeEdit.show()
                self.processP2RangeEdit.show()
                self.processP3RangeEdit.show()
                self.processP4RangeEdit.show()

            self.label.show()
            self.label_27.show()
            self.procTypeCombo.show()
            self.pidCombo.show()
            
            self.label_18.show()
            self.label_19.show()
            self.label_20.show()
            self.label_21.show()
            self.contrP1Slider.show()
            self.contrP2Slider.show()
            self.contrP3Slider.show()
            self.contrP4Slider.show()
            
            self.contrP1Edit.show()
            self.contrP2Edit.show()
            self.contrP3Edit.show()
            self.contrP4Edit.show()
            
            self.contrP1RangeButton.show()
            self.contrP2RangeButton.show()
            self.contrP3RangeButton.show()
            self.contrP4RangeButton.show()
            
            self.contrP1RangeEdit.show()
            self.contrP2RangeEdit.show()
            self.contrP3RangeEdit.show()
            self.contrP4RangeEdit.show()  


    def hideStepOrRamp(self, object):
        ''' Hides/shows the step/ramp
        inputs according to the input type. '''

        step_condition = self.inputType_group.id(object) == -3
        ramp_condition = self.inputType_group.id(object) == -2

        if step_condition:
            self.stepMag_edit.show()
            self.stepMagnitudeLabel_2.show()
            self.rampSlope_edit.hide()
            self.rampSlopeLabel_2.hide()

        elif ramp_condition:
            self.stepMag_edit.hide()
            self.stepMagnitudeLabel_2.hide()
            self.rampSlope_edit.show()
            self.rampSlopeLabel_2.show()

        else:
            print("Something went wrong while trying \
                to hide step or ramp input.")


    def run_simulation(self):
        ''' Runst a discrete simulation. '''
        
        # 1. Get control mode
        if (self.current_mode == "Regulatory control"):
            mode = "reg"
        elif (self.current_mode == "Servo control"):
            mode = "servo"
        elif (self.current_mode == "Servo and regulatory"):
            mode = "both"
        else:
            mode = "process"

        # 2. Get process type
        processType = self.procTypeCombo.currentText()

        # 3. Get PID type
        pidType = self.pidCombo.currentText()

        # 4. Get input type
        if (self.current_inputType == -3):
            inputType = "step"
        else:
            inputType = "ramp"
        
        # 5. Get simulation type
        if (self.current_simulationType == "Discrete"):
            simulationType = "discrete"
        else:
            simulationType = "realTime"

        # 6. Get simulation time
        try:
            time = float(self.simulationTimeEdit.text())
            t = np.linspace(0,time,5001)
        except:
            qtw.QMessageBox.information(self, "Invalid simulation time", "Please enter a number")

        # 7. Get time units
        timeUnits = self.timeUnits_combo.currentText()

        # 8. Get input time
        try:
            timeIn = float(self.inputTimeEdit.text())
        except:
            qtw.QMessageBox.information(self, "Invalid input time", "Please enter a number")
        
        # 9. Get step or ramp value
        if (inputType == "step"):
            inputValue = float(self.stepMag_edit.text())
        else:
            inputValue = float(self.rampSlope_edit.text())

        # 10. Get process parameters
        numeratorP = self.process_num_edit.text()
        numP = conversion(numeratorP)

        denominatorP = self.process_den_edit.text()
        denP = conversion(denominatorP)

        if numP == '' or denP == '':
            qtw.QMessageBox.information(self, "Error", 
                    "Process numerator and denominator must not be empty")
            return # Simulation finishes

        if len(numP)>len(denP):
            qtw.QMessageBox.information(self, "Error", 
                    "Process numerator must not be greater than the denominator")
            return # Simulation finishes

        A = control.tf(numP, denP)

        dT = self.process_deadTime_edit.text()
        L = float(dT)
        numPade, denPade = control.pade(L,n=int(self.padeDegree_edit.text()))
        Pade = control.tf(numPade, denPade)
        P = A*Pade

        if (mode == "process"):
            lab = 'Process'
            tP, yP, inP = response(P, inputValue, timeIn, t, inputType)
            self.results = indexes('P', inputType, inP, yP, tP, 0)        
            graph(timeUnits, lab, inP, tP, yP)
            self.openGraphsWindow(img1='img/temp/fig1.png')
        else:
            # Controller data
            numeratorC = self.contr_num_edit.text()
            numC = conversion(numeratorC)

            denominatorC = self.contr_den_edit.text()
            denC = conversion(denominatorC)

            if numC == '' or denC == '':
                qtw.QMessageBox.information(self, "Error", 
                        "Controller numerator and denominator must not be empty")
                return # Simulation finishes

            if len(numC)>len(denC):
                qtw.QMessageBox.information(self, "Error", 
                        "Controller numerator must not be greater than the denominator")
                return # Simulation finishes

            C = control.tf(numC, denC)

            # Calculated transfer functions
            MYR = (C*P)/(1+C*P)
            MYD = P/(1+C*P)
            S = 1/(1+C*P)
            UR = C/(1+C*P)
            UD = (-C*P)/(1+C*P)

            if (mode == 'servo'):
                lab = 'servo'
                tServo,yServo,inServo = response(MYR,inputValue,timeIn,t,inputType)
                tUR,yUR,inUR = response(UR,inputValue,timeIn,t,inputType)
                tP,yP,inP = response(P,inputValue,timeIn,t,inputType)
                # self.results = indexes('P',inputType,inP,yP,tP,0)
                self.results = indexes('MYR',inputType,inServo,yServo,tServo,yUR)
                graph(timeUnits, lab,inServo,tServo,yServo,tUR,yUR,tP,yP)
                
            elif (mode == 'reg'):
                lab = 'reg'
                tReg,yReg,inReg = response(MYD,inputValue,timeIn,t,inputType)
                tUD,yUD,inUD = response(UD,inputValue,timeIn,t,inputType)
                tP,yP,inP = response(P,inputValue,timeIn,t,inputType)
                # self.results = indexes('P',inputType,inP,yP,tP,0)
                self.results = indexes('MYD',inputType,inReg,yReg,tReg,yUD)
                graph(timeUnits, lab,inReg,tReg,yReg,tUD,yUD,tP,yP)
            elif (mode == 'both'):
                lab = 'both'
                tServo,yServo,inServo = response(MYR,inputValue,timeIn,t,inputType)
                tUR,yUR,inUR = response(UR,inputValue,timeIn,t,inputType)
                tReg,yReg,inReg = response(MYD,inputValue,timeIn,t,inputType)
                tUD,yUD,inUD = response(UD,inputValue,timeIn,t,inputType)
                self.results = indexes('MYR',inputType,inServo,yServo,tServo,yUR)
                self.results += ("\n\n\n" + indexes('MYD',inputType,inReg,yReg,tReg,yUD))
                graph(timeUnits, lab,inServo,tServo,yServo,tUR,yUR,tReg,yReg,tUD,yUD)                
            # This bode function is used to obtain the magnitude of S:
            m,p,w = control.bode_plot(S,plot=False)  
            Ms = max(m)  # Ms is the maximum value of the magnitude array.
            Ms = round(Ms,7)
            self.results = self.results + "\n\nMAXIMUM SENSITIVITY:\nMs = {}".format(Ms) 
            if (mode == 'servo' or mode == 'reg'):
                self.openGraphsWindow('img/temp/fig1.png', 'img/temp/fig2.png', 
                                      'img/temp/fig3.png')
            else:
                self.openGraphsWindow('img/temp/fig1.png', 'img/temp/fig2.png', 
                                      'img/temp/fig3.png', 'img/temp/fig4.png')


    def runRealTimeSimulation(self, first=False):
        ''' Runs a real time simulation. '''

        # 1. Get control mode
        if (self.current_mode == "Regulatory control"):
            mode = "reg"
        elif (self.current_mode == "Servo control"):
            mode = "servo"
        elif (self.current_mode == "Servo and regulatory"):
            mode = "both"
        else:
            mode = "process"

        # 2. Get process type
        processType = self.procTypeCombo.currentText()

        # 3. Get PID type
        pidType = self.pidCombo.currentText()

        # Get simulation time
        try:
            time = float(self.simulationTimeEdit.text())
            t = np.linspace(0,time,5001)
        except:
            qtw.QMessageBox.information(self, "Invalid value", "Please enter a number")

        # Get time units
        timeUnits = self.timeUnits_combo.currentText()

        # Get input type
        if (self.current_inputType == -3):
            inputType = "step"
        else:
            inputType = "ramp"

        # Get step or ramp value
        if (inputType == "step"):
            inputValue = float(self.stepMag_edit.text())
        else:
            inputValue = float(self.rampSlope_edit.text())

        # Input time
        try:
            timeIn = float(self.inputTimeEdit.text())
        except:
            qtw.QMessageBox.information(self, "Invalid value", "Please enter a number")

        # Process data
        if (processType == "Standard"):
            dc_gain = float(self.processP1Edit.text())
            natural_frequency = float(self.processP2Edit.text())
            damping_factor = float(self.processP3Edit.text())

            # Numerator            
            numeratorP = [dc_gain*(natural_frequency**2)]

            # Denominator
            denominatorP = [1,(2*natural_frequency*damping_factor),natural_frequency**2]
        elif processType == "Alternative":
            dc_gain = float(self.processP1Edit.text())
            time_constant = float(self.processP2Edit.text())
            a = float(self.processP3Edit.text())

            # Numerator
            numeratorP = [dc_gain]

            # Denominator
            denominatorP = [a*time_constant**2,time_constant*(a+1),1]
            pole0 = control.tf([1],[a*time_constant,1])
            pole1 = control.tf([1],[time_constant,1])   
        elif processType == "Custom":
            numeratorP = self.process_num_edit.text()
            numP = conversion(numeratorP)
            denominatorP = self.process_den_edit.text()
            denP = conversion(denominatorP)
            if numP == '' or denP == '':
                qtw.QMessageBox.information(self, "Error", 
                        "Process numerator and denominator must not be empty")
                return # Simulation finishes

            if len(numP)>len(denP):
                qtw.QMessageBox.information(self, "Error", 
                        "Process numerator must not be greater than the denominator")
                return # Simulation finishes

        if (processType != "Custom"):
            numP = [float(x) for x in numeratorP]
            denP = [float(x) for x in denominatorP]
        
        A = control.tf(numP,denP)
        # Tiempo muerto
        dT = self.processP4Edit.text()
        L = float(dT)
        numPade, denPade = control.pade(L,n=int(self.padeDegree_edit.text()))
        Pade = control.tf(numPade,denPade)
        P = A*Pade

        try:
            if mode == 'process':
                if self.rtSimulationActive or first:
                    lab = 'Process'        
                    tP,yP,inP = response(P,inputValue,timeIn,t,inputType)
                    self.results = indexes('P',inputType,inP,yP,tP,0)
                    graph(timeUnits, lab,inP,tP,yP)
                    self.openGraphsWindow(img1='img/temp/fig1.png', realTime=True)  
            else:
                if (pidType == "Standard"):
                    numeratorC = [((float(self.contrP4Edit.text())+1)*float(self.contrP1Edit.text())*float(self.contrP3Edit.text())*float(self.contrP2Edit.text())),float(self.contrP1Edit.text())*((float(self.contrP4Edit.text())*float(self.contrP3Edit.text())+float(self.contrP2Edit.text()))),float(self.contrP1Edit.text())]
                if (pidType == "Parallel"):
                    numeratorC = [float(self.contrP3Edit.text())*(float(self.contrP4Edit.text())*float(self.contrP1Edit.text())+1), float(self.contrP1Edit.text())+(float(self.contrP4Edit.text())*float(self.contrP2Edit.text())*float(self.contrP3Edit.text())), float(self.contrP2Edit.text())]
                if (pidType == "Series"):
                    numeratorC = [float(self.contrP1Edit.text())*float(self.contrP2Edit.text())*float(self.contrP3Edit.text()), float(self.contrP2Edit.text())+float(self.contrP3Edit.text()), float(self.contrP1Edit.text())]
                numC = [float(x) for x in numeratorC]

                if(pidType == "Standard"):
                    denC = [(float(self.contrP4Edit.text())*float(self.contrP3Edit.text())*float(self.contrP2Edit.text())),float(self.contrP2Edit.text()),0]
                    # eqC2 = control.tf([1],[float(self.contrP2Edit.text()),0])
                    # eqC3 = control.tf([float(self.contrP3Edit.text()),0],[float(self.contrP4Edit.text())*float(self.contrP3Edit.text()),1])
                elif(pidType == "Parallel"):
                    denC = [float(self.contrP4Edit.text())*float(self.contrP3Edit.text()),1,0]
                    # eqC1 = control.tf([float(self.contrP2Edit.text())],[1,0])
                    # eqC2 = control.tf([float(self.contrP3Edit.text()),0],[float(self.contrP3Edit.text())*float(self.contrP4Edit.text()),1])
                elif(pidType == "Series"):
                    denC = [float(self.contrP4Edit.text())*float(self.contrP2Edit.text())*float(self.contrP3Edit.text()), float(self.contrP2Edit.text()), 0]
                    # eqC1 = control.tf([self.contrP2Edit.text(),1],[self.contrP2Edit.text(),0])
                    # eqC2 = control.tf([self.contrP3Edit.text(),1],[self.contrP3Edit.text()*self.contrP4Edit.text(),1])
                try:
                    C = control.tf(numC,denC)
                except: 
                    C = control.tf(numC,[1])

                # Calculated transfer functions.
                MYR = (C*P)/(1+C*P)
                MYD = P/(1+C*P)
                S = 1/(1+C*P)
                UR = C/(1+C*P)
                UD = (-C*P)/(1+C*P)

                # System response and performance indexes computation.
                if self.rtSimulationActive or first:
                    if mode == 'servo':                
                            lab = 'servo'
                            tP,yP,inP = response(P,inputValue,timeIn,t,inputType)
                            tServo,yServo,inServo = response(MYR,inputValue,timeIn,t,inputType)
                            tUR,yUR,inUR = response(UR,inputValue,timeIn,t,inputType)
                            # self.results = indexes('P',inputType,inP,yP,tP,0)
                            self.results = indexes('MYR',inputType,inServo,yServo,tServo,yUR)
                            graph(timeUnits, lab,inServo,tServo,yServo,tUR,yUR,tP,yP)
                            
                    elif mode == 'reg':
                            lab = 'reg'
                            tP,yP,inP = response(P,inputValue,timeIn,t,inputType)
                            tReg,yReg,inReg = response(MYD,inputValue,timeIn,t,inputType)
                            tUD,yUD,inUD = response(UD,inputValue,timeIn,t,inputType)
                            # self.results = indexes('P',inputType,inP,yP,tP,0)
                            self.results = indexes('MYD',inputType,inReg,yReg,tReg,yUD)
                            graph(timeUnits, lab,inReg,tReg,yReg,tUD,yUD,tP,yP)
                            
                    elif mode == 'both':
                            lab = 'both'
                            tServo,yServo,inServo = response(MYR,inputValue,timeIn,t,inputType)
                            tUR,yUR,inUR = response(UR,inputValue,timeIn,t,inputType)
                            tReg,yReg,inReg = response(MYD,inputValue,timeIn,t,inputType)
                            tUD,yUD,inUD = response(UD,inputValue,timeIn,t,inputType)
                            self.results = indexes('MYR',inputType,inServo,yServo,tServo,yUR)
                            self.results += ("\n\n\n" +  indexes('MYD',inputType,inReg,yReg,tReg,yUD))
                            graph(timeUnits, lab,inServo,tServo,yServo,tUR,yUR,tReg,yReg,tUD,yUD)
                            
                    # Closed Loop maximum sensitivity Ms.
                    m,p,w = control.bode_plot(S,plot=False)  # This bode function is used to obtain the magnitude of S.
                    Ms = max(m)  # Ms is the maximum value of the magnitude array.
                    Ms = round(Ms,7)
                    self.results = self.results + "\n\nMAXIMUM SENSITIVITY:\nMs = {}".format(Ms) 
                    if (mode == 'servo' or mode == 'reg'):
                        self.openGraphsWindow(img1='img/temp/fig1.png', img2='img/temp/fig2.png',
                                                img3='img/temp/fig3.png', img4='img/transparent.png', 
                                                realTime=True)
                    else:
                        self.openGraphsWindow(img1='img/temp/fig1.png', img2='img/temp/fig2.png',
                                                img3='img/temp/fig3.png', img4='img/temp/fig4.png', 
                                                realTime=True)
        except:
            qtw.QMessageBox.information(self, "Simulation error", 
                            "Something went wrong with the simulation")


    def showSliderValue(self, lineEditObject, value, rangeList):
        ''' Shows the numerical value of the sliders
        on the corresponding line edits.''' 

        granularity = rangeList[2]

        if granularity >= 1:
            lineEditObject.setText(str(value))
        else:
            print(str(value*granularity))
            lineEditObject.setText(str(value*granularity))


    def updateSliderValue(self, sliderObject, value, rangeList):
        ''' Shows the value of the line edits 
        on the corresponding sliders. '''

        granularity = rangeList[2]
        if granularity >= 1:
            sliderObject.setValue(int(value))
        else:
            sliderObject.setValue(round(value/granularity))


    def getLineEditValue(self, lineEdit):
        ''' Obtains the value of a process 
        or controller parameter line edit. '''

        if lineEdit.text() == '':
            return 0
        else:
            try:
                return float(lineEdit.text())
            except:
                qtw.QMessageBox.information(self, "Incorrect value", "Please enter a number")
                return 0


    def setSliderRange(self, slider, values):
        ''' Sets the range and granularity
        of a slider. '''

        try:
            slider.setMinimum(values[0])
            slider.setMaximum(values[1])
            slider.setSingleStep(values[2])
        except:
            pass


    def getRangeValues(self, lineEdit, slider):
        ''' Obtains the user-defined ranges for
        the sliders. '''

        values = lineEdit.text().split(",")
        if (len(values) != 3):
            qtw.QMessageBox.information(self, "Incorrect value", "Please enter 3 values")
            return
        floatValues = [float(value) for value in values]
        intValues = []
        
        try:
            test = int(values[2])
            intValues.append(int(floatValues[0]))
            intValues.append(int(floatValues[1]))
            intValues.append(int(floatValues[2]))            
        except:
            intValues.append(int(floatValues[0]/floatValues[2]))
            intValues.append(int(floatValues[1]/floatValues[2]))
            intValues.append(1)

        if slider == 'p1':
            self.processP1SliderValues = floatValues
        elif slider == 'p2':
            self.processP2SliderValues = floatValues
        elif slider == 'p3':
            self.processP3SliderValues = floatValues
        elif slider == 'p4':
            self.processP4SliderValues = floatValues
        elif slider == 'c1':
            self.contrP1SliderValues = floatValues
        elif slider == 'c2':
            self.contrP2SliderValues = floatValues
        elif slider == 'c3':
            self.contrP3SliderValues = floatValues
        elif slider == 'c4':
            self.contrP4SliderValues = floatValues

        return intValues


if __name__ == '__main__':
    app = qtw.QApplication([])
    window = SimulatorWindow()
    window.show()
    app.setStyle('Fusion')
    app.exec_()