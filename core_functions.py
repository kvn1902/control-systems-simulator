import numpy as np
import control as co
from scipy import integrate
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg,NavigationToolbar2Tk
from matplotlib.figure import Figure
import platform


# Sets font size for all figures
plt.rcParams.update({'font.size': 18})

# Figures
fig1 = Figure(figsize=(5,5),dpi=100)
fig1, ax1 = plt.subplots()
fig2 = Figure(figsize=(5,5),dpi=100)
fig2, ax2 = plt.subplots()
fig3 = Figure(figsize=(5,5),dpi=100)
fig3, ax3 = plt.subplots()
fig4 = Figure(figsize=(5,5),dpi=100)
fig4, ax4 = plt.subplots()
fig1.set_tight_layout(True)
fig2.set_tight_layout(True)
fig3.set_tight_layout(True)
fig4.set_tight_layout(True)


def conversion(input): 
   ''' Converts the input from string to a float array.
    Returns: final float array. '''
   try:
      input = str(input)
      caracters = "[]"
      string = input.replace(caracters[0],"").replace(caracters[1],"")  # Remove '[]' from string.
      list = string.split(',')  # List created from the string. String-type values.
      final = [float(x) for x in list]  # List of floats created using the first list.
      return final
   except:
      return ''

def response(tf,ku,tin,time,signal):
   ''' Computes the desired step or ramp input of magnitude ku and applied on t=tin.
   Computes the system response to the desired input.
   Returns: ta,ya,ua: time and response values and the input computed. '''

   ntp = len(time)
   u = np.zeros(ntp)
   if signal == 'step':
   # Step with amplitude ku applied on t=tin.
      for k in range(ntp):
         u[k] = 0. if time[k] < tin else ku
      ta,ya = co.forced_response (tf,time,u)
   elif signal == 'ramp':
      # Ramp with slope ku aaplied on t=tin.
      for k in range(ntp):
         u[k] = 0. if time[k] < tin else ku*time[k]
      ta,ya = co.forced_response (tf,time,u)
   return ta,ya,u


def indexes(FT,inputName,yinput,y,t,ua):
   ''' Computes de IAE, ISE and ITAE of a given system response.
   Also computes de control effort TVu of a given control signal.
   Returns: None '''

   if FT == 'MYD': error = -y
   else: error = yinput-y
   iae = integrate.trapezoid(np.abs(error),t)
   ise = integrate.trapezoid(error**2,t)
   itae = integrate.trapezoid(np.abs(error)*t,t)
   if FT != 'P': TV = np.sum(np.abs(np.diff(ua)));TV = round(TV,7)
   iae = round(iae,7);ise = round(ise,7);itae = round(itae,7)
   inputName = str(inputName).upper()

   if FT == 'MYR': 
     C = "SERVO CONTROL \n{} INPUT".format(inputName)
     T = 'TVur'
     label0 = 'IAEr'
     label1 = 'ISEr'
     label2 = 'ITAEr'
   elif FT == 'MYD':
     C = """REGULATORY CONTROL \n{} INPUT""".format(inputName)
     T = 'TVud'
     label0 = 'IAEd'
     label1 = 'ISEd'
     label2 = 'ITAEd'
   else:
     C = 'REACTION CURVE'
     label0 = 'IAE'
     label1 = 'ISE'
     label2 = 'ITAE'
   if FT != 'P':
      TV = np.sum(np.abs(np.diff(ua)))
      TV = round(TV,7)  
      results = "{}\n\n".format(C) + \
         "{} = {}\n\n".format(T, TV) + \
         "{} = {}\n\n".format(label0, iae) + \
         "{} = {}\n\n".format(label1, ise) + \
         "{} = {}".format(label2, itae)

   else:
      results = """
      {}
      """.format(C)
   return results


def graph(units, etq,uin,t1,y1,t2=0,y2=0,t3=0,y3=0,t4=0,y4=0):
   ''' Plots the system simulations using Matplotlib.
   Automatically sets the corresponding labels according to the control mode used.
   Plots the system response (or reaction curve) and control signals in separate Figures.
   Returns: None '''

   ax1.clear();ax2.clear();ax3.clear();ax4.clear()

   if etq == 'Process':
      lab1 = 'y(t)'; labIn1 = 'r(t)'
      ax1.plot(t1,uin,':m',label=labIn1)
      ax1.plot(t1,y1,'-b',label=lab1)
      ax1.legend()
      ax1.set_xlabel('Time ({})'.format(units));ax1.set_ylabel('Amplitude');ax1.set_title('Process Natural Response')      
      fig1.savefig('img/temp/fig1.png')
      plt.close(fig1)

   elif etq == 'both': 
      lab1 = 'yr(t)'; lab2 = 'ur(t)'
      lab3 = 'yd(t)'; lab4 = 'ud(t)'
      labIn1 = 'r(t)'; labIn2 = 'd(t)'
      ax1.plot(t1,uin,':m',label=labIn1)
      ax1.plot(t1,y1,'-b',label=lab1);ax1.legend()
      ax2.plot(t1,uin,':m',label=labIn1)
      ax2.plot(t2,y2,'-b',label=lab2);ax2.legend()
      ax3.plot(t1,uin,':m',label=labIn2)
      ax3.plot(t3,y3,'-b',label=lab3);ax3.legend()
      ax4.plot(t1,uin,':m',label=labIn2)
      ax4.plot(t4,y4,'-b',label=lab4);ax4.legend()
      ax1.set_xlabel('Time ({})'.format(units));ax1.set_ylabel('Amplitude');ax1.set_title('System Response (Servo)')
      ax2.set_xlabel('Time ({})'.format(units));ax2.set_ylabel('Amplitude');ax2.set_title('Controller Response (Servo)')
      ax3.set_xlabel('Time ({})'.format(units));ax3.set_ylabel('Amplitude');ax3.set_title('System Response(Regulatory)')
      ax4.set_xlabel('Time ({})'.format(units));ax4.set_ylabel('Amplitude');ax4.set_title('Controller Response(Regulatory)')
      fig1.savefig('img/temp/fig1.png')
      fig2.savefig('img/temp/fig2.png')
      fig3.savefig('img/temp/fig3.png')
      fig4.savefig('img/temp/fig4.png')
      plt.close(fig1)
      plt.close(fig2)
      plt.close(fig3)
      plt.close(fig4)

   else: 
      if (etq == 'servo'): 
         lab1='yr(t)'
         lab2='ur(t)'
         labIn1='r(t)'
      else: 
         lab1='yd(t)'
         lab2='ud(t)'
         labIn1='d(t)'
      lab3 = 'y(t)'; labIn3 = 'r(t)'
      ax1.plot(t1,uin,':m',label=labIn1)
      ax1.plot(t1,y1,'-b',label=lab1);ax1.legend()
      ax2.plot(t1,uin,':m',label=labIn1)
      ax2.plot(t2,y2,'-b',label=lab2);ax2.legend()
      ax3.plot(t1,uin,':m',label=labIn3)
      ax3.plot(t3,y3,'-b',label=lab3);ax3.legend()
      ax1.set_xlabel('Time ({})'.format(units));ax1.set_ylabel('Amplitude');ax1.set_title('System Response')
      ax2.set_xlabel('Time ({})'.format(units));ax2.set_ylabel('Amplitude');ax2.set_title('Controller Response')
      ax3.set_xlabel('Time ({})'.format(units));ax3.set_ylabel('Amplitude');ax3.set_title('Process Natural Response')      
      fig1.savefig('img/temp/fig1.png')
      fig2.savefig('img/temp/fig2.png')
      fig3.savefig('img/temp/fig3.png')
      plt.close(fig1)
      plt.close(fig2)
      plt.close(fig3)