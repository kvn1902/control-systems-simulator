all: venv

venv: 
	pyuic5 uiFiles/gui.ui -o mainWindow.py
	pyuic5 uiFiles/graphics_window_process.ui -o simulationWindow_process.py
	pyuic5 uiFiles/graphics_window_1mode.ui -o simulationWindow_1mode.py
	pyuic5 uiFiles/graphics_window_both.ui -o simulationWindow_both.py
	pyuic5 uiFiles/info_window.ui -o infoWindow.py
	sed -i 's/QFrame.StyledPanel/QFrame.Panel/g' mainWindow.py
	sed -i 's/QFrame.StyledPanel/QFrame.Panel/g' simulationWindow_process.py
	sed -i 's/QFrame.StyledPanel/QFrame.Panel/g' simulationWindow_1mode.py
	sed -i 's/QFrame.StyledPanel/QFrame.Panel/g' simulationWindow_both.py
	sed -i 's/Ui_GraphicsWindow/Ui_GraphicsWindow_process/g' simulationWindow_process.py
	sed -i 's/Ui_GraphicsWindow/Ui_GraphicsWindow_1mode/g' simulationWindow_1mode.py
	sed -i 's/Ui_GraphicsWindow/Ui_GraphicsWindow_both/g' simulationWindow_both.py