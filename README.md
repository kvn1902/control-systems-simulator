# Control Systems Simulator

*** This project is in progress ***

This simulator is part of a graduation project from the University of Costa Rica (UCR). It is an improvement of a simulator created as part of a previous graduation project: https://gitlab.com/zetavB/Control-Systems-Simulator.

The simulator allows you to simulate a variety of control systems by modifying both controller and plant parameters. It displays the results graphically along with performance indicators.

## Requirements

The project was made using Python 3.8.10. It is recommended to use a virtual environment:

```
python3 -m venv <environment_name>
```

To activate it on Linux:

```
source <environment_name>/bin/activate
```

To activate it on Windows:

```
<environment_name>\Scripts\activate
```

To install the requirements:

```
pip3 install -r requirements.txt
```

To deactivate the environmet:

```
deactivate
```

## Run the simulator

```
python3 main.py
```

## Common issues

### "RuntimeError: pdflatex is not installed"

Download and intall LaTeX: https://www.latex-project.org/get/


## Simulation examples

### Discrete simulation

This mode lets you explicitly set the numerator and denominator for process and controller.

![IMAGE_DESCRIPTION](img/discrete1.PNG)

![IMAGE_DESCRIPTION](img/discrete2.PNG)

### Real time simulation

This mode lets you change the process and plant parameters through sliders, and see their effect in real time.

![IMAGE_DESCRIPTION](img/realtime1.PNG)

![IMAGE_DESCRIPTION](img/realtime2.PNG)
